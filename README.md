# Genshin Artifact Simulator
- A program which simulates farming and upgrading artifacts in Genshin Impact. 
- I plan on adding more features to make the experience as similar to in-game as possible.
- Currently each run only yields 1 5-star artifact.

## Running the Application (2 Methods)
1. Download the repository and run 'main.py' from your terminal.
2. Download the [executable](https://bitbucket.org/trwstin/genas/downloads/Genshin_Artifact_Simulator.exe) and run directly.

## Suggestions
- If you have any suggestions (features, optimizations), please contact me via Discord @ Wambo#0126.
